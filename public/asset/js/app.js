$(document).ready(function () {
  const productId = $("#product_id");
  const productName = $("#product_name");
  const productPrice = $("#product_price");
  const productType = $("#product_type");
  const submitButton = $("#submit_btn");
  const deleteButton = $("#delete_button");
  const myForm = $("#my_form");
  const delButton = $("#del_product");
  const uniqueError = $("#error");
  const clearMessage = $("#closeError");

  uniqueError.hide();

  let showValue = $("#show_value");

  productType.on("change", function (event) {
    // console.log(event.target.value);
    showValue.html(getProductType[event.target.value]);
  });

  getProductType = {
    Dvd: `<div class="form-group row">
    <label for="dvd_value" class="col-sm-2 col-form-label">Size (MB)</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="dvd_value" placeholder="" name="dvd_value">
      <small class="form-text text-muted">Please provide dimension in MB format</small>
    </div>

  </div>`,
    Dimension: `<main>
  <div class="form-group row">
    <label for="furniture_height" class="col-sm-2 col-form-label">Height (CM)</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="furniture_height" name="furniture_height" placeholder="">
    </div>
  </div>
  <div class="form-group row">
    <label for="furniture_width" class="col-sm-2 col-form-label">Width (CM)</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="furniture_width" name="furniture_width" placeholder="">
    </div>
  </div>
  <div class="form-group row">
    <label for="furniture_length" class="col-sm-2 col-form-label">Length (CM)</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="furniture_length" name="furniture_length" placeholder="">
      <small class="form-text text-muted">Please provide dimension in H*W*L format</small>
    </div>
  </div>


</main>`,
    Book: `<div class="form-group row">
<label for="book_value" class="col-sm-2 col-form-label">Weight (KG)</label>
<div class="col-sm-10">
  <input type="text" name="book_value" class="form-control" id="book_value" placeholder="">
  <small class="form-text text-muted">Please provide dimension in Weight format</small>
</div>

</div>`,
  };

  // perform ajax Request
  submitButton.on("click", function () {
    // let form_data = new FormData();
    // console.log("Hello");
    if (productId.val() == "") {
      productId.addClass("is-invalid");
      return;
    } else {
      productId.removeClass("is-invalid").addClass("is-valid");
    }
    if (productName.val() == "") {
      productName.addClass("is-invalid");
      return;
    } else {
      productName.removeClass("is-invalid").addClass("is-valid");
    }
    if (productPrice.val() == "") {
      productPrice.addClass("is-invalid");
      return;
    } else {
      productPrice.removeClass("is-invalid").addClass("is-valid");
    }
    if (productType.val() == "") {
      productType.addClass("is-invalid");
      return;
    } else {
      productType.removeClass("is-invalid").addClass("is-valid");
    }
    $.ajax({
      method: "POST",
      url: "../private/response.php",
      data: $("form").serialize(),
    }).done(function (msg) {
      console.log("Data Saved: " + msg);
      if (msg == "Error") {
        uniqueError.fadeIn(1000);
      } else {
        setTimeout(() => {
          window.location.href = "index.php";
        }, 1500);
      }
    });
  });

  // delete button
  deleteButton.on("click", function () {
    console.log("Hello");
    let id = [];
    $("#product_id:checked").each(function (i) {
      id[i] = $(this).val();
    });
    $.ajax({
      method: "POST",
      url: "../private/delete.php",
      data: { id: id },
    }).done(function (msg) {
      if (msg) {
        setTimeout(() => {
          window.location.href = "index.php";
        }, 1500);
      }
    });
  });

  clearMessage.on("click", function () {
    uniqueError.fadeOut(1000);
  });
});
