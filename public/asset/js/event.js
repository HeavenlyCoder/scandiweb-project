$(document).ready(function () {
  var submitBtn = $("#submit");

  var formTag = $("#form");

  var name = $("#name");

  // trigger submit event
  submitBtn.on("click", function (event) {
    // prevent default behavior
    event.preventDefault();

    // change the text of the button
    submitBtn.text("Loading...");

    // disable the button
    submitBtn.attr("disabled", true);

    setTimeout(() => {
      // eable the button
      submitBtn.attr("disabled", false);

      submitBtn.text("Submit");
      alert(name.val());
    }, 5000);
  });

  var errorMessage = $("#errorMessage");
  errorMessage.hide();

  // using the onkeyup event
  name.on("keyup", function () {
    // console.log($(this).val());

    var maxLength = 10;
    if ($(this).val().length > maxLength) {
      errorMessage.fadeIn(2000);
      errorMessage.text("You've exceed maximum number which is " + maxLength);
      // disable the button
      submitBtn.attr("disabled", true);
    } else {
      errorMessage.fadeOut(2000);
      // enable the button
      submitBtn.attr("disabled", false);
    }
    $(".card-text").text($(this).val());
  });
});
