<?php require_once "shared/header.php"; ?>

<!-- heading -->
<div class="container mt-5">
  <div class="top-head d-flex justify-content-between">
    <h4>New Product</h4>
    <div class="btn-group" role="group" aria-label="Button group">
      <a href="index.php" class="btn btn-danger">Cancel</a>
      <button class="btn btn-info" id="submit_btn">Submit Request</button>
    </div>
  </div>
</div>
<section class="product">
  <div class="container">
    <div class="row">
      <div class="col-md-8 mx-auto">
        <div class="card">
          <div class="card-body">
            <div class="alert alert-danger alert-dismissible" role="alert" id="error">
              <h4 class="alert-heading">Error Occured</h4>
              Product Id must be unique
              <button type="button" id="closeError" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form id="my_form">
              <!-- product ID -->
              <div class="form-group row">
                <label for="product_id" class="col-sm-2 col-form-label">SKU</label>
                <div class="col-sm-10">
                  <input type="text" required class="form-control " id="product_id" placeholder="" name="product_id">
                </div>
              </div>
              <!-- product name -->
              <div class="form-group row">
                <label for="product_name" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                  <input type="text" required class="form-control" id="product_name" placeholder="" name="product_name">
                </div>
              </div>
              <!-- product price -->
              <div class="form-group row">
                <label for="product_price" class="col-sm-2 col-form-label">Price ($)</label>
                <div class="col-sm-10">
                  <input type="text" required class="form-control" id="product_price" placeholder=""
                    name="product_price">
                </div>
              </div>
              <!-- product type -->
              <div class="form-group row">
                <label for="product_type" class="col-sm-2 col-form-label">Product Type</label>
                <div class="col-sm-10">
                  <select id="product_type" class="form-control" required name="product_type">
                    <option value="">Pick product type</option>
                    <option value="Dvd" id="dvd">DVD_disc</option>
                    <option value="Book" id="book">Book</option>
                    <option value="Dimension" id="furniture">Furniture</option>
                  </select>
                </div>

              </div>
              <!-- for DVD -->
              <div id="show_value"></div>
              <!-- for FURNITURE -->
              <!-- <main>
                <div class="form-group row">
                  <label for="furniture_height" class="col-sm-2 col-form-label">Height (CM)</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="furniture_height" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="furniture_width" class="col-sm-2 col-form-label">Width (CM)</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="furniture_width" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="furniture_length" class="col-sm-2 col-form-label">Length (CM)</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="furniture_length" placeholder="">
                    <small class="form-text text-muted">Please provide dimension in H*W*L format</small>
                  </div>
                </div>


              </main> -->

              <!-- for book -->
              <!-- <div class="form-group row">
                <label for="book_value" class="col-sm-2 col-form-label">Weight (KG)</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="book_value" placeholder="">
                  <small class="form-text text-muted">Please provide dimension in Weight format</small>
                </div>

              </div> -->

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<?php require_once "shared/footer.php"; ?>