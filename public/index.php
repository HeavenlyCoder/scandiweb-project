<?php require_once "shared/header.php"; ?>

<!-- heading -->
<div class="container mt-5">
  <div class="top-head d-flex justify-content-between">
    <h4>All Products</h4>
    <div class="btn-group" role="group" aria-label="Button group">
      <a href="new.php" class="btn btn-info">Add Product</a>
      <button type="button" class="btn btn-danger" id="delete_button">Mass Delete</button>
    </div>
  </div>
</div>
<section class="product">
  <div class="container">
    <div class="row">
      <?php

        // find all products
        $products = Product::findAll();
        foreach ($products as $product): ?>
      <div class="col-md-3 mb-4 col-6">
        <div class="card">
          <div class="card-body">
            <div class="checked-item">
              <div class="form-check">
                <input id="product_id" class="form-check-input" type="checkbox" name="id[]"
                  value="<?php print h($product->id); ?>">
              </div>
            </div>
            <div class="items text-center">
              <h5 class="card-title ml-2" id="unique_id">
                <?php print h($product->product_id); ?>
              </h5>
              <p class=" card-text" id="name">
                <?php print h($product->name); ?>
              </p>
              <p class="card-text" id="price">
                <?php print h(number_format($product->price, 2) . " $"); ?>
              </p>
              <p class="card-text" id="size">
                <?php print h($product->attributes . ": " . $product->value); ?>
              </p>
            </div>

          </div>
        </div>
      </div>

      <?php endforeach ?>
    </div>
  </div>
</section>

<?php require_once "shared/footer.php"; ?>