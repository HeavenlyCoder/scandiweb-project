<?php

// import the initialize file
require "initialize.php";

$args = [];

$error = [];

$args['sku'] = $_POST['product_id'] ?? '';
$args['name'] = $_POST['product_name'] ?? '';
$args['price'] = $_POST['product_price'] ?? '';
$args['type'] = $_POST['product_type'] ?? '';

$realValue = "";

switch ($args['type']) {
  case 'Book':
    $realValue = $_POST['book_value'] . "KG";
    break;

  case 'Dvd':
    $realValue = $_POST['dvd_value'] . " MB";
    break;
    
  default:
    $realValue = $_POST['furniture_height'] . '*' . $_POST['furniture_width'] . '*' .$_POST['furniture_length'];
    break;
}

$args['value'] = $realValue;

$validateSku = Product::findProductBySku($args['sku']);

if ($validateSku) {
  # code...
  print "Error";
  return false;
}

$product = new Product($args);

$product->makeUppercase($product->product_id);

$result = $product->create();

if ($result) {
  # code...
  
  // redirect back to product listing
  // redirect_to("../public/index.php");
  // print_r($_POST);
  print "success";

} else {
  print "error";
}


?>