<?php

class Product {

  // START ACTIVE RECORD PATTERN

  static public $database;

  static public function setDatabase($database){
    self::$database = $database;
    
  }

  static public function findBySql($sql) {
    $result = self::$database->query($sql);
    // check records exist
    if (!$result) {
      exit("Database query failed");
    }

    // turn the result into object
    $objectArray = [];

    while ($record = $result->fetch_array()) {
      $objectArray[] = self::instantiateRecord($record);
    }

    // free result
    // $result->free();

    return $objectArray;

  }

  static public function findAll() {
    $sql = "SELECT * FROM products";
    return self::findBySql($sql);
  }

  static public function instantiateRecord($record) {

    $objectRecord = new Product;

    foreach ($record as $property => $value) {

      if (property_exists($objectRecord, $property)) {
        $objectRecord->$property = $value;
      }
    }

    return $objectRecord;
  }

  public function create() {
    $sql = "INSERT INTO products (";
    $sql .= "product_id, name, price, attributes, value";
    $sql .= ") VALUES (";
    $sql .= "'" . $this->product_id ."',";
    $sql .= "'" . $this->name . "', ";
    $sql .= "'" . $this->price ."', ";
    $sql .= "'" . $this->attributes ."', ";
    $sql .= "'" . $this->value ."'";
    $sql .= ")";

    $result = self::$database->query($sql);
    if ($result) {
      $this->id = self::$database->insert_id;
    }
    
    return $result;
  }

  static public function delete($id = []) {

    foreach ($id as $value) {

      $sql = "DELETE FROM products ";
      $sql .= "WHERE id = '" . self::$database->escape_string($value) ."'";
      $sql .= "LIMIT 1";
      
      $query = self::$database->query($sql);
    }
    if ($query) {
      return true;
    }
    // return $value;
  }

  static public function findProductBySku($value) {
    
    $sql = "SELECT * FROM products ";
    $sql .= "WHERE product_id = '" . self::$database->escape_string($value) ."'";
    $query = self::findBySql($sql);

    if (!empty($query)) {
      return array_shift($query);
    } else {
      return false;
    }

  }


  // END ACTIVE RECORD PATTERN

  public $id;
  public $name;
  public $product_id;
  public $price;
  public $attributes;
  public $value;

  public function __construct($args = [])
  {
    $this->product_id = $args['sku'] ?? '';
    $this->name = $args['name'] ?? '';
    $this->price = $args['price'] ?? '';
    $this->attributes = $args['type'] ?? '';
    $this->value = $args['value'] ?? '';
  } 

  public function makeUppercase($string) {
    return strtoupper($string);
  }
  

}



?>