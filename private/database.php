<?php

require_once "db_credentials.php";

//  create a database connection
function dbConnect() {
  $connection = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);

  // confirm database connection
  confirmDbConnect($connection);

  // return the connection
  return $connection;
}

function confirmDbConnect($connection) {

  if ($connection->connect_errno) {
    $msg .= "Database connection failed ";
    $msg .= $connection->connect_error ;
    $msg .= " ( " . $connection->connect_errno . " )";
    exit($msg);
  }

  
}