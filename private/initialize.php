<?php
  ob_start(); // output buffering is turned on

  session_start(); // turn on sessions

  require_once('database.php');
  // useful function
  require_once "functions.php";
  // bring in PRODUCT class
  require_once "class/Product.php";

  // require_once "validation_functions.php";
  

  $db = dbConnect();

  Product::setDatabase($db);

?>